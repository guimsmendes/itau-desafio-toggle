# Desafio: Serviço de Feature Toggle
## Descrição
Este repositório contém o código de _kickstart_ do desafio de criação de um Serviço de Feature Toggle simples.

Mais instruções podem ser encontradas no arquivo `enunciado.md`.

# Status

# Tabela de Conteúdo

# Descrição

# Tecnologias utilizadas

# Layout/Deploy da aplicação

# Pré-requisitos

# Dependências e Libs instaladas
# Como rodar a aplicação
# Como rodar Testes
# Database
# Solução de Problemas
# Observability

# Links úteis